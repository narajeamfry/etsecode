package dades;

/**
 * 
 * Guarda una posici� dins la matriu.
 * 
 * @author marc.lopezb
 *
 */
public class Posicio {

	int x, y;
	
	public Posicio()
	{
		x = 0;
		y = 0;
	}
	
	public Posicio(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public Posicio(Posicio p)
	{
		this.x = p.x;
		this.y = p.y;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public boolean equals(Object p)
	{
		if(p instanceof Posicio)
		{
			Posicio aux = (Posicio)p;
			return (aux.x == x && aux.y == y);
		}
		else
			return false;
	}
	
	public int hashCode() {
		int hash = 0;
		hash += x*1000;
		hash += y;
	    return hash;
	}
}
