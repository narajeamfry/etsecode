package dades;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import joc.Carta;

/**
 * 
 * Aquesta classe guarda una matriu de cartes. T� un tamany i una llista
 * de les cartes que cont�.
 * 
 * @author marc.lopezb
 *
 */
public class MatriuCartes {

	/*
	 * Dades a guardar:
	 * 
	 *   -Tamany de la matriu
	 *   -Cartes que cont�
	 *   -Carta triada actualment
	 *   
	 */
	
	public int amplada, alcada;
	protected Map<Posicio, Carta> cartes;
	protected Posicio tria;
	
	public MatriuCartes(int nombreCartes)
	{
		// Actualitzar els tamanys de la matriu autom�ticament.
		this.calcularTamanys(nombreCartes);
		
		cartes = new HashMap<Posicio, Carta>();
		tria = null;
		
		omplirMatriu();
	}

	private void omplirMatriu() {
		
		ArrayList<Posicio> posicions = new ArrayList<Posicio>();
		
		for(int i=0; i<alcada; i++)
		{
			for(int j=0; j<amplada; j++)
			{
				posicions.add(new Posicio(j, i));
			}
		}
		
		Collections.shuffle(posicions);
		
		int i=0;
		
		for(Posicio p : posicions)
		{
			cartes.put(p, new Carta((i++)/2));
		}
		
	}

	/**
	 * 
	 * Actualitza el tamany de la matriu segons el nombre de cartes.
	 * 
	 * @param nombreCartes Nombre de cartes de la partida
	 */
	private void calcularTamanys(int nombreCartes) {
		switch(nombreCartes)
		{
		case 12:
			amplada = 4;
			alcada = 3;
		case 24:
			amplada = 6;
			alcada = 4;
		case 48:
			amplada = 8;
			alcada = 6;
		default:
			amplada = 4;
			alcada = 3;
		}
	}
	
	public int getAmplada() {
		return amplada;
	}

	public void setAmplada(int amplada) {
		this.amplada = amplada;
	}

	public int getAlcada() {
		return alcada;
	}

	public void setAl�ada(int al�ada) {
		this.alcada = al�ada;
	}

	public String toString()
	{
		String result = "";
		
		for(int i=0; i<alcada; i++)
		{
			for(int j=0; j<amplada; j++)
			{
				result = result.concat(cartes.get(new Posicio(j, i)) + " ");
			}
			result = result.concat("\n");
		}
		
		return result;
	}
	
	private String taulerAmagat(Posicio p)
	{
		String result = "";
		
		for(int i=0; i<alcada; i++)
		{
			for(int j=0; j<amplada; j++)
			{
				if(cartes.get(new Posicio(j, i)).estaViva())
				{
					if(tria != null && tria.equals(new Posicio(j, i)))
						result = result.concat(cartes.get(new Posicio(j, i)).toString()+" ");
					else
					{
						if(p != null && p.equals(new Posicio(j, i)))
							result = result.concat(cartes.get(new Posicio(j, i)).toString() + " ");
						else
							result = result.concat(" * ");
					}
						
				}
				else
					result = result.concat("   ");
			}
			result = result.concat("\n");
		}
		
		return result;
	}
	
	public String taulerAmagatAmbTria(Posicio p)
	{
		return taulerAmagat(p);
	}
	
	public String taulerAmagatSenseTria()
	{
		return taulerAmagat(null);
	}

	public boolean quedenMoviments() {
		for(int i=0; i<alcada; i++)
		{
			for(int j=0; j<amplada; j++)
			{
				if(cartes.get(new Posicio(j, i)).estaViva())
					return true;
			}
		}
		return false;
	}
	
	public void triarCarta(Posicio p)
	{
		if(p!= null && cartes.get(p).estaViva())
			this.tria = new Posicio(p);
		else
			this.tria = null;
	}
	
	public boolean cartaTriada()
	{
		return (tria != null);
	}
	
	public Carta getCarta(Posicio p)
	{
		return cartes.get(p);
	}
	
	public Carta getCartaTriada()
	{
		if(tria!=null)
			return cartes.get(tria);
		else
			return null;
	}
	
	public Posicio getPosicioTriada()
	{
		return tria;
	}
	
	public void mataCarta(Posicio p)
	{
		cartes.get(p).mataCarta();
	}
	
	public void mataCartaTriada()
	{
		if(tria!=null)
			cartes.get(tria).mataCarta();
	}
	
}
