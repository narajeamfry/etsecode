package main;

import dades.Posicio;
import exceptions.EndGameException;
import exceptions.NivellIncorrecteException;
import exceptions.PosicioIncorrectaException;
import gui.FinestraJoc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import joc.TaulerJoc;

public class MainJoc {

	
	/* Dades a guardar del joc 
	 * 
	 *   -Un tauler de joc
	 *   -La finestra de joc?
	 *   -El torn actual
	 *   -La resposta del joc (perd o no torn)
	 * 
	 * */
	
	TaulerJoc tauler;
	FinestraJoc finestra;
	/**
	 *  Ser� 0 o 1 depenent del jugador que toqui (1 o 2 respectivament).
	 */
	int tornActual;
	String mostraResultat;
	
	public static void main(String[] args) {
		
		// Inicialitza el joc!
		MainJoc joc = new MainJoc();
		
		try{

			// Aqui es demanen els noms dels jugadors i el nombre de cartes. 
			// Al final del m�tode es genera la finestra de joc.
			joc.inicialitzarJoc();
		
			// Es mostren les cartes boca amunt i es polsa algun bot�.
			joc.iniciarPartida();
		
			joc.bucleJoc();
		
			joc.acabarJoc();
			joc.mostrarAgraiment();
		}
		catch(EndGameException ege)
		{
			joc.mostrarAgraiment();
			return;
		}
		
	}

	private void bucleJoc() {
		while(tauler.quedenMoviments())
		{
			System.out.println(mostraResultat);
			System.out.println("Torn del jugador "+(tauler.getTorn()+1)+": Quina casella vols triar? (Escriu \"fila columna\", de 1 a n)");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
			boolean correcte = false;
			
			while(!correcte)
			{
				try {
					StringTokenizer st = new StringTokenizer(br.readLine());
					
					if(st.countTokens() != 2)
						throw new PosicioIncorrectaException();
					int x = Integer.parseInt(st.nextToken())-1;
					int y = Integer.parseInt(st.nextToken())-1;
					
					if(x == 40 + 1 && y == 42 -2)
					{
						System.out.println("This card is the answer to all questions on the universe.\n");
					}
					
					if(x<0 || x > tauler.getAmplada() || y<0 || y>tauler.getAlcada())
						throw new PosicioIncorrectaException();
					mostraResultat = tauler.triaCarta(new Posicio(y, x));
					correcte = true;
				} catch (IOException e) {
					System.out.println("No es pot llegir, torna a intentar.");
				}
				catch (NumberFormatException nfe)
				{
					System.out.println("No es un nombre, torna a intentar.");
				} catch (PosicioIncorrectaException pie) {
					System.out.println("La posicio es incorrecta (fora del tauler o no te dos elements)");
				}
			}
			
			
		}
	}

	private void acabarJoc() {
		int guanyador = tauler.getGuanyador();
		
		System.out.println("S'ha acabat la partida! El guanyador es el jugador "+(guanyador+1));
	}

	private void mostrarAgraiment() {
		System.out.println("Moltes gr�cies per jugar! :)");
	}

	/**
	 * 
	 * Demana a l'usuari el nombre de parelles per a la partida.
	 * 
	 * @return Un enter positiu o -1 si no s'ha llegit res.
	 * 
	 */
	private int demanarNivell() {
		
		System.out.println("Quin nivell vols? (f per F�cil, m per Mitj�, d per Dif�cil)");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String input;
		int resultat = -1;
		boolean correcte = false;
		
		while(!correcte)
		{
			try
			{
				input = br.readLine();
				
				switch(input)
				{
				case "f":
					resultat = 11;
					break;
				case "m":
					resultat = 24;
					break;
				case "d":
					resultat = 48;
					break;
				default:
					throw new NivellIncorrecteException();
				}
				
				correcte = true;
			}
			catch(IOException ioe)
			{
				System.out.println("Entrada no v�lida! Torna a provar.");
			}
			catch (NivellIncorrecteException e) {
				System.out.println("Aquest nivell no est� disponible :( Torna a provar");
			}
		}
		
		if(resultat > 50)
		{
			resultat = 50;
			System.out.println("Hi ha massa parelles (el m�xim �s 50).");
		}
		else if(resultat < 3)
		{
			resultat = -1;
			System.out.println("Hi ha massa poques parelles (el m�nim �s 3).");
		}
		
		return resultat;
		
	}

	private void iniciarPartida() {
		System.out.println(tauler.toString());
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Instruccions: Dos jugadors es tornen per girar cartes.");
		System.out.println("Exemple de torn: ");
		System.out.println("1 1 -> Gira la carta a dalt a l'esquerra.");
		
		System.out.println("Prem enter quan estiguis llest.");
		try
		{
			br.readLine();
			Runtime.getRuntime().exec("cls");
		}
		catch(IOException ioe)
		{}
		
		mostraResultat = tauler.taulerAmagat();
		
	}

	private void inicialitzarJoc() throws EndGameException {

		
		int cartes = demanarNivell();
		
		// Demanar noms jugadors (opcional)
		
		// Crear Tauler de Joc
		this.tauler = new TaulerJoc(cartes);
	}
	
	

}
