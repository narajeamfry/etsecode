package joc;

public class Carta {

	/*
	 * Dades a guardar:
	 * 
	 *   -ID de la carta
	 *   -Imatge?
	 *   
	 */
	
	int idCarta;
	boolean viva;
	
	public Carta(int id)
	{
		this.idCarta = id;
		this.viva = true;
	}
	
	public String toString()
	{
		String resultat = Integer.toString(idCarta);
		
		if(resultat.length() == 1)
			resultat = " ".concat(resultat);
		
		return resultat;
		
	}
	
	public void mataCarta()
	{
		this.viva = false;
	}
	
	public boolean estaViva()
	{
		return viva;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof Carta)
		{
			return idCarta == ((Carta)o).idCarta;
		}
		else
			return false;
	}
	
}
