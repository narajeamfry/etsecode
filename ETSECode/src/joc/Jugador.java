package joc;

public class Jugador {

	/*
	 * Dades a guardar:
	 * 
	 *   -Puntuacio
	 * 
	 */
	int puntuacio;
	
	public Jugador()
	{
		puntuacio = 0;
	}
	
	public void afegirPunt() {
		puntuacio++;
	}
	
	public int getPunts()
	{
		return puntuacio;
	}

}
