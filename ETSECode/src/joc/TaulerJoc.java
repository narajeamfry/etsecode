package joc;

import dades.MatriuCartes;
import dades.Posicio;

/**
 * 
 * Aquesta Classe cont� les cartes de la partida. El tamany es calcula autom�ticament
 * segons el nombre de cartes del tauler.
 * 
 * @author marc.lopezb
 *
 */
public class TaulerJoc {
	
	/*
	 * Dades a guardar:
	 * 
	 *   -Una matriu de cartes
	 *   -Jugadors
	 *   
	 */

	Jugador[] jugadors;
	int tornActual;
	MatriuCartes tauler;
	
	public TaulerJoc(int nombreCartes)
	{
		
		tauler = new MatriuCartes(nombreCartes);
		
		jugadors = new Jugador[] { new Jugador(), new Jugador() };
		
		tornActual = 0;
		
	}
	
	public boolean quedenMoviments()
	{
		return tauler.quedenMoviments();
	}
	
	public String toString()
	{
		String resultat = tauler.toString();
		resultat = resultat.concat("\nJugador 1: "+jugadors[0].getPunts()+" - Jugador 2: "+jugadors[1].getPunts());
		
		return resultat;
	}
	
	public String taulerAmagat()
	{
		String resultat = tauler.taulerAmagatSenseTria();
		resultat = resultat.concat("\nJugador 1: "+jugadors[0].getPunts()+" - Jugador 2: "+jugadors[1].getPunts());
		
		return resultat;
	}
	
	/**
	 * 
	 * Tria una carta i la mostra al tauler. Si n'hi ha una triada, comprova si coincideix.
	 * 
	 * Retorna si el jugador actual perd el torn o no.
	 * 
	 * Per exemple, si tria dues cartes i s�n diferents, retorna true.
	 * 
	 * @param p Posicio a comprovar
	 * @return True si el jugador perd torn, false otherwise.
	 */
	public String triaCarta(Posicio p)
	{
		String resultat = "";
		
		if(tauler.cartaTriada())
		{
			if(p.equals(tauler.getPosicioTriada()))
			{
				// Si es la mateixa carta no fem res.
			}
			else
			{
				if(tauler.getCarta(p).equals(tauler.getCartaTriada()))
				{
					tauler.mataCarta(p);
					tauler.mataCartaTriada();
					jugadors[tornActual].afegirPunt();
					tauler.triarCarta(null);
				}
				else
				{
					if(tauler.getCarta(p).estaViva())
					{
						tornActual = (tornActual + 1) % 2;
						resultat = resultat.concat(tauler.taulerAmagatAmbTria(p));
						resultat = resultat.concat("--------- Fi del torn!\n");
						tauler.triarCarta(null);
					}
				}
			}
		}
		else
		{
			tauler.triarCarta(p);
		}
		
		resultat = resultat.concat(taulerAmagat());
		
		return resultat;
	}
	
	public int veurePuntuacio(int jugador)
	{
		return jugadors[jugador].getPunts();
	}

	public int getGuanyador()
	{
		if(jugadors[0].getPunts() > jugadors[1].getPunts())
			return 0;
		else if(jugadors[0].getPunts() < jugadors[1].getPunts())
			return 1;
		else
			return -1;
	}

	public int getTorn() {
		return tornActual;
	}
	
	public int getAmplada() {
		return tauler.getAmplada();
	}
	
	public int getAlcada() {
		return tauler.getAlcada();
	}
}
